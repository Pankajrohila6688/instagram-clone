import React, { useEffect, useState } from 'react';
import faker from 'faker';
import Story from './Story';
import { useSession } from "next-auth/react";
import { PlusCircleIcon } from "@heroicons/react/outline";

function Stories() {

    const [suggestions, setSuggestions] = useState([]);
    const { data: session } = useSession();

    useEffect(() => {
        const suggestions = [...Array(20)].map((_, i) => ({
            ...faker.helpers.contextualCard(),
            id: i,  
        }));
        setSuggestions(suggestions)
    },[])

    return (
        <div className="flex space-x-2 p-6 bg-white mt-8 border-gray-200 border rounded-sm overflow-x-scroll scrollbar-hide">
            {session && (
                <div className="flex">
                    <Story img={session.user.image} username={session.user.username} />
                    <PlusCircleIcon className="h-6 md:inline-flex cursor-pointer -ml-6 mt-8 z-10 bg-blue-500 rounded-full text-white" />
                </div>
            )}

            {suggestions.map(profile => (
                <Story 
                    key={profile.id} 
                    img={profile.avatar} 
                    username={profile.username} 
                />
            ))}
        </div>
    )
}

export default Stories;

