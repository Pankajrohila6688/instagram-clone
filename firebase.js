// Import the functions you need from the SDKs you need
import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD9OEdVlt0Qt2G4oyDsBgOHY8F6nkO9RN4",
  authDomain: "instagram-2-ca0d3.firebaseapp.com",
  projectId: "instagram-2-ca0d3",
  storageBucket: "instagram-2-ca0d3.appspot.com",
  messagingSenderId: "109743360671",
  appId: "1:109743360671:web:f628217f5ed881debc70aa",
  measurementId: "G-3SZTJJWQVW"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const storage = getStorage();

export { app, db, storage };